# https://github.com/inossidabile/wash_out

class UcmController < ApplicationController
  # Please replace `include WashOut::SOAP` with `soap_service`
  # in your controllers if you are upgrading from a version before 0.8.5.
  include WashOut::SOAP
  soap_service namespace: 'cus:WashOut'

  soap_action 'update_people',
              args: { people: [{ eidmGuid: :string, firstName: :string, lastName: :string, email: :string, language: :string, country: :string }] },
              return: { status: :integer, message: :string }
  def update_people
    params[:people].each_with_index do |person, idx|
      Rails.logger.error("Person #{idx + 1}: #{person.inspect}")
    end

    UcmService.update_people(params[:people])
    render soap: {status: 200, message: "Users updated." }
  rescue => error
    Rails.logger.error("Error: #{error.message}\n#{error.backtrace.join("\n")}")
    raise SOAPError, error.message
  end

  soap_action 'personmaster',
              args: { person: [{ eidmGuid: :string, firstName: :string, lastName: :string, email: :string, language: :string, country: :string }] },
              return: { status: :integer, message: :string }
  def personmaster
    params[:people].each_with_index do |person, idx|
      Rails.logger.error("Person #{idx + 1}: #{person.inspect}")
    end

    UcmService.update_people(params[:people])
    render soap: {status: 200, message: "Users updated." }
  rescue => error
    Rails.logger.error("Error: #{error.message}\n#{error.backtrace.join("\n")}")
    raise SOAPError, error.message
  end

end

=begin

client = Savon::Client.new(wsdl: "http://localhost:3000/ucm/wsdl")
client.operations -> GET "/ucm/wsdl"

 result = client.call(:update_people, message: { people: 
                                      [
                                      {
                                        eidm_guid: '12345',
                                        first_name: 'Albert',
                                        last_name: 'Einstein',
                                        email: 'aeinstein@universe.com',
                                        language: 'de',
                                        country: 'DE'
                                      },
                                      {
                                        eidm_guid: '56789',
                                        first_name: 'Adolf',
                                        last_name: 'Hitler',
                                        email: 'ahitler@universe.com',
                                        language: 'de',
                                        country: 'DE'
                                      }, 
                                    
                                      ] })

  result.body # or result.to_hash                              

=end
