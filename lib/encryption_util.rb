class EncryptionUtil
  class << self
    def load_keys(public_key_file, private_key_file)
      @@public_key  = OpenSSL::PKey::RSA.new File.read public_key_file
      @@private_key = OpenSSL::PKey::RSA.new File.read private_key_file
      [@@public_key, @@private_key]
    end

    def encrypt(text)
      keys_existence_check
      Base64.encode64(@@public_key.public_encrypt(text))
    end

    def decrypt(encrypted_text)
      keys_existence_check
      @@private_key.private_decrypt(Base64.decode64(encrypted_text))
    end

    private
    def keys_existence_check
      raise 'You have to load keys by calling EncryptionUtil.load_keys(public_key_file, private_key_file).' unless (@@public_key && @@private_key)
    end
  end
end