# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20080926225304) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "cats", force: true do |t|
    t.integer "age"
    t.string  "name",        limit: 40, null: false
    t.string  "animal_type", limit: 40
    t.string  "cat_attr",    limit: 60
    t.integer "stripes"
  end

  create_table "claws", force: true do |t|
    t.integer "cat_id",     null: false
    t.integer "claw_value", null: false
  end

  add_index "claws", ["cat_id"], name: "index_claws_on_cat_id", using: :btree

  create_table "dogs", force: true do |t|
    t.integer "age"
    t.string  "name",     limit: 40, null: false
    t.string  "dog_attr", limit: 60
    t.integer "pack_id"
  end

  add_index "dogs", ["pack_id"], name: "index_dogs_on_pack_id", using: :btree

  create_table "users", force: true do |t|
    t.string   "login"
    t.string   "email"
    t.string   "crypted_password",          limit: 40
    t.string   "salt",                      limit: 40
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "remember_token"
    t.datetime "remember_token_expires_at"
  end

end
