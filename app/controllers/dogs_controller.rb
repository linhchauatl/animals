class DogsController < ApplicationController
  include DogsHelper
  
  def index
    # logger.debug("request.ssl? = #{request.ssl?} - #{request.public_methods.sort.join("\n")} - #{request.url} ")
    @dogs = DogsService.dog_list
    @not_tabby_cats, @tabby_cats = CatsService.cat_list
    # Rails.logger.error controller.send(:_layout)
    render template: '/index'
  end
 
  def ajax_show
    dog = DogsService.find_dog_by_id(params[:id].to_i)
    # If we use this "require turbolinks", after the partial is rendered, it will issue another request to /
    render partial: 'details', locals: {dog: dog}
  end
  
  def ajax_pack_show
    pack_id = params[:id].to_i
    dogs = DogsService.find_pack_dogs_by_pack_id(pack_id)
    render partial: 'pack_details', locals: {dogs: dogs, pack_id: pack_id }
  end

  def destroy
    DogsService.delete_dog(params[:id].to_i)
    redirect_to action: 'index'
  end

  def edit
    prepare_for_edit(params[:id].to_i)
  end

  def update
    update_status = DogsService.update_dog(params[:id].to_i, dog_params)
    if update_status
      flash[:notice] = 'Dog was successfully updated.'
      redirect_to action: 'index'
    else
      flash[:notice] = 'Cannot update the dog with information you submitted.'
      prepare_for_edit(params[:id].to_i)
      render action: 'edit'
    end
  end

  def new
    @existing_pack_ids = DogsService.find_existing_pack_ids
  end

  def create
    save_status = DogsService.save_dog(dog_params)
    if save_status
      flash[:notice] = 'Dog was successfully created.'
      redirect_to action: 'index'
    else
      flash[:notice] = 'Cannot create a dog with information you submitted.'
      @existing_pack_ids = DogsService.find_existing_pack_ids
      render action: 'new'
    end
  end
  
private
  def dog_params
    params.require(:dog).permit(:name, :age, :dog_attr, :pack_id)
  end

  def prepare_for_edit(id)
    prepare_results = DogsService.prepare_for_edit(id)
    @dog = prepare_results[:dog]
    @existing_pack_ids = prepare_results[:existing_pack_ids]
  end
  
end
