# https://github.com/elastic/elasticsearch-rails/tree/master/elasticsearch-model
# https://github.com/elastic/elasticsearch-rails/tree/master/elasticsearch-persistence  The ActiveRecord Pattern
config = ConfigService.load_config('elasticsearch.yml')[Rails.env]
Elasticsearch::Model.client =  Elasticsearch::Client.new( url: config.url, log: true)

# Dog.import(force: true)
# response = Dog.search('3')
# fd = response.results.first
# fd.as_json
# fd.as_json['_source']
