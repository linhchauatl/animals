require 'rails_helper'

describe DogsService do
  context 'dog_list' do
    it 'calls Dog.all' do
      expect(Dog).to receive(:all)
      DogsService.dog_list
    end
  end

  context 'find_dog_by_id' do
    before :all do
      @dogs = []
      3.times do |i|
        @dogs << create(:dog, name: "Dog Test #{i}", pack_id: 200)
      end
    end

    it 'returns the correct dog' do
      dog = DogsService.find_dog_by_id((@dogs[0].id))
      [:id, :name, :pack_id, :dog_attr].each do |attribute|
        expect(dog.send(attribute)).to eql(@dogs[0].send(attribute))
      end
    end

    it 'calls populate_dogs_pack on the returned dog' do
      dog = DogsService.find_dog_by_id((@dogs[0].id))
      expect(dog.dogs_pack).to match_array(@dogs - [dog])
    end

    it 'raises error if it cannot find the dog with given id' do
      expect { DogsService.find_dog_by_id(0) }.to raise_error(ActiveRecord::RecordNotFound) 
    end
  end

  context 'delete_dog' do
    before :each do
      @dog = create(:dog)
    end

    it 'deletes the dog' do
      DogsService.delete_dog(@dog.id)
      # expect(Dog.find_by_id(@dog.id)).to be nil <= This line works correctly too
      expect { Dog.find(@dog.id) }.to raise_error(ActiveRecord::RecordNotFound) 
    end

    it 'raises error if it cannot find the dog with given id' do
      expect { DogsService.delete_dog(0) }.to raise_error(ActiveRecord::RecordNotFound) 
    end
  end

  context 'update_dog' do
    # update_dog(id, params_dog)
    before :each do
      @dog = create(:dog, name: 'Dog 1', age: '1', dog_attr: 'Belongs to pack 1', pack_id: '1')
      @dog_params = { name: 'Dog update 1', age: '5', dog_attr: 'Belongs to pack 100', pack_id: '100'}
    end

    it 'updates the dog correctly' do
      dog = DogsService.update_dog(@dog.id, @dog_params)
      expect(dog.name).to eql('Dog update 1')
      expect(dog.age).to eql(5)
      expect(dog.dog_attr).to eql('Belongs to pack 100')
      expect(dog.pack_id).to eql(100)
    end

    it 'raises error if it cannot find the dog with given id' do
      expect { DogsService.update_dog(0, {}) }.to raise_error(ActiveRecord::RecordNotFound) 
    end
  end

  context 'find_existing_pack_ids' do
    it 'returns all the exising pack_ids' do
      Dog.delete_all
      3.times do |i|
        create(:dog, name: "Dog Test #{i}", pack_id: (i + 1))
      end

      pack_ids = DogsService.find_existing_pack_ids
      expect(pack_ids).to match_array([1, 2, 3])
    end
  end

  context 'find_pack_dogs_by_pack_id' do
    before :all do
      @dogs = []
      3.times do |i|
        @dogs << create(:dog, name: "Dog Test #{i}", pack_id: 201)
      end

      @extra_dog = create(:dog, name: "Dog Test Extra", pack_id: 202)
    end

    it 'returns all the dogs in the pack with pack_id' do
      dogs = DogsService.find_pack_dogs_by_pack_id(201)
      expect(dogs).to match_array(@dogs)
    end

    it 'returns an empty array if there is no dog in the pack with pack_id' do
      dogs = DogsService.find_pack_dogs_by_pack_id(0)
      expect(dogs).to match_array([])
    end
  end

  context 'prepare_for_edit' do
    before :all do
      Dog.delete_all

      @dogs = []
      3.times do |i|
        @dogs << create(:dog, name: "Dog Test #{i}", pack_id: 1)
      end

      @extra_dog = create(:dog, name: "Dog Test Extra", pack_id: 2)
    end

    it 'returns a hash that contains the dog and a list of existing_pack_ids' do
      result = DogsService.prepare_for_edit(@dogs[0].id)
      expect(result[:dog]).to eql(@dogs[0])
    end

    it 'raises error if the dog cannot be found' do
      expect { DogsService.prepare_for_edit(0) }.to raise_error(ActiveRecord::RecordNotFound) 
    end
  end

  context 'save_dog' do
    before :all do
      @dog_params = { name: 'Dog create 1', age: '1', dog_attr: 'Belongs to pack 300', pack_id: '300'}
    end

    it 'create the dog' do
      dog = DogsService.save_dog(@dog_params)
      expect(dog.id).not_to be nil
      expect(dog.name).to eql('Dog create 1')
      expect(dog.age).to eql(1)
      expect(dog.dog_attr).to eql('Belongs to pack 300')
      expect(dog.pack_id).to eql(300)
    end
  end
end