source 'https://rubygems.org'


# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.1.6'
# Use postgresql as the database for Active Record
gem 'pg'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 4.0.3'

# http://getbootstrap.com/customize/
# http://getbootstrap.com/css/
# http://www.cheatography.com/masonjo/cheat-sheets/bootstrap/
# http://www.taesup.com/bootstrap/
gem 'bootstrap-sass' 
gem 'autoprefixer-rails'

# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .js.coffee assets and views
gem 'coffee-rails', '~> 4.0.0'
# See https://github.com/sstephenson/execjs#readme for more supported runtimes
# gem 'therubyracer',  platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'
gem 'jquery-ui-rails'
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0',          group: :doc

# Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
gem 'spring',        group: :development

# Use ActiveModel has_secure_password
gem 'bcrypt', '~> 3.1.7'

# Use unicorn as the app server
gem 'unicorn'

# Use Capistrano for deployment
gem 'capistrano-rails'
gem 'haml' # http://haml.info/docs/yardoc/file.REFERENCE.html

group :development, :test do
  gem 'factory_girl_rails' # http://rubydoc.info/gems/factory_girl/file/GETTING_STARTED.md
  gem 'pry-rails'

  # https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
  # https://www.relishapp.com/rspec/rspec-rails/v/3-1/docs
  # http://myronmars.to/n/dev-blog/2014/05/notable-changes-in-rspec-3
  # https://relishapp.com/rspec/docs/upgrade
  gem 'rspec-rails' # rails g rspec:install
  gem 'rest-client'
  gem 'cucumber-rails', require: false # rails g cucumber:install
  gem 'database_cleaner'
end

gem 'wash_out'

# https://github.com/savonrb/gyoku <= Add attributes to XML
gem 'savon'
gem 'object_cacheable'

# ActiveMQ security:
# http://activemq.apache.org/security.html

# https://github.com/stompgem/stomp
# client = Stomp::Client.new("user", "pass", "localhost", 61613)
# client.publish("/queue/mine", "hello world!")
# client.subscribe("/queue/mine") do |msg|
#   p msg
# end

# client = Stomp::Client.new('admin', 'admin', 'localhost', 61613)
# client.publish('ActiveMQ.Advisory.MasterBroke', 'Hello!')

# msgs = []
# client.subscribe('ActiveMQ.Advisory.MasterBroke') do |msg|
#   msgs << msg
# end
  
# Change activemq password: http://activemq.apache.org/version-5-getting-started.html
gem 'stomp'

# http://hone.herokuapp.com/resque/2012/08/21/resque-signals.html
# Add this to Rakefile: 
#  require 'resque/tasks'
#  task 'resque:setup' => :environment
# env TERM_CHILD=1 QUEUE=* bundle exec rake resque:work
# http://tutorials.jumpstartlab.com/topics/performance/background_jobs.html
gem 'resque'

gem 'config_service'
gem 'elasticsearch-model'
gem 'elasticsearch-rails'
