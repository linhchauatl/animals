module CatsHelper
  def cat_types(cat, disabled = true)
    all_cat_types = Cat.find_by_sql("select distinct animal_type from cats").map {|c| (c.animal_type.nil?)? 'Cat': c.animal_type }.uniq
    select( "cat", "animal_type", all_cat_types, {:selected => (cat)? cat.animal_type : all_cat_types.first},
             {:disabled => disabled} )
  end 
  
  def cat_type_observer(cat)
    observe_field "cat_animal_type",
      { :url => {:controller => 'cats',
                 :action => 'ajax_show_stripes'},
        :with => "'value=' + value",
        :update => 'stripesInput',
        :without_script_tag => true
      }
  end
end
