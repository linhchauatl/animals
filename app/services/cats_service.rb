class CatsService
  def self.cat_list
    cats =  Cat.all
    tabby_cats =  TabbyCat.all
    not_tabby_cats =  cats - tabby_cats
    [not_tabby_cats, tabby_cats]
  end
  
  def self.find_cat_by_id(id)
    cat = Cat.find_by_id(id)
  end
  
  def self.delete_cat(id)
    cat = Cat.find_by_id(id).destroy
  end
  
  def self.update_cat(id, params_cat, params_claw)
    cat = Cat.find_by_id(id)
    Claw.destroy_all("id in (#{cat.claws.map{|x| x.id}.join(",")})") if !cat.claws.empty?
    
    cat.claws = create_cat_claws(params_claw)
    cat.update_attributes(params_cat)
  end
  
  def self.save_cat(params)
    cat = eval(params[:cat][:animal_type]).new(params[:cat])
    cat.claws = create_cat_claws( params[:claw])
    cat.save
  end
  
private
  
  def self.create_cat_claws(params_claw)
    claw_values = params_claw[:claws].split("\n")
    new_claws = []
    for claw_value in claw_values
      claw = Claw.new(:cat_id => id, :claw_value => claw_value )
      new_claws << claw
    end
    new_claws
  end
  
end