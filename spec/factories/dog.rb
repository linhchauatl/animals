FactoryGirl.define do
  factory :dog do
    name "Dog Test #{Time.now.to_i}"
    age 2
    dog_attr 'Belongs to pack 1'
    pack_id 1
  end
end