Rails.application.routes.draw do
  get '/', :controller => 'dogs', :action => 'index'
  
  resources :users
  resource :session
  
  get '/dogs/ajax_show'      => 'dogs#ajax_show'
  get '/dogs/ajax_pack_show' => 'dogs#ajax_pack_show'
  resources :dogs

  get '/cats/ajax_show' => 'cats#ajax_show'
  resources :cats
  
  get '/fish/language_auto_comlete' => 'fish#language_auto_comlete'
  post '/fish/create_asset' => 'fish#create_asset'

  wash_out :ucm
end
