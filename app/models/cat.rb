class Cat < ActiveRecord::Base
  has_many :claws, :dependent => :delete_all
  
  def self.inheritance_column
    'animal_type'
  end
end
