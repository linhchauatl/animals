class Dog < ActiveRecord::Base
  include Elasticsearch::Model

  attr_accessor :dogs_pack
  
  validates :name, length: { minimum: 1, maximum: 40 }
  validates :age, presence: true, numericality: true
  validates_numericality_of :pack_id, allow_nil: true # old Rails
  validates_length_of :dog_attr, maximum: 60 # old Rails
  
  def populate_dogs_pack
    @dogs_pack = Dog.all.where("pack_id = ? and id <> ?", self.pack_id, self.id)
  end  
end
