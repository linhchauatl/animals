require 'rails_helper'
require_relative '../../lib/encryption_util'

describe EncryptionUtil do
  before :all do
    @key_path = File.expand_path('./config')
    @text = 'This is a plain text.'

    @encrypted_text = <<ENCRYPTED
ie3HIaI+2668UFb2inwJ0cv57PQzkihA1A7xNA6AGM9RT9w3VE72v+bRqlmT
21Nvo0ZNxLylzLdHqxgnpxty3Jc3sQNXbfgelhOuNJxysssyO22ApfRaWu+w
t/vOMFXYwhSUD4KX3aOtYUyOyNivhCl5eEyBlRiWO8C9/4rR9Vk3AGmV3v7a
JSDkMaSMKwLHpP7FC3Ez7u87jWDHkW3lUfEoYy8rr/GGaXjBoa3J8VlLpIFP
AUoKMRoGigAExhCVJV9YulknRnVtIQ6hCMkHiPxAR2jUm3AxPMxhDV9bYC+0
OqajdJW5/u5B1zH+HRPAzgRI7vx/LiPodqAXIXycA5EcrvTlUJ4hCUYs6drx
VZuenfpXEkgx12MuHmjMu+PBUtHCnKoJy1T4C9KaA9wxgI9STEV+auelCm4P
mvIgzoypk4CXG+MFlY7RXT1OsQSDGb4I3vkQe4eKyFjWz7DQ5OIBXyN8HoHI
NqjafSG8uS8rDgGaH3SIjTQksyOxjHdDroFWCYDIFLNFeT7xTinHkdBdQGs7
EpHySEVlxKRvpCxUr3mWG6E1wMtcoY8o0/UF8dqS20B7WpTK1pn7Prxs5tZy
3wA4d3F9/tYIgrnyU91eWoeMvj0Zh8ynGH62kdpJPJvTuQgyK6ZbLD8lQyiV
EhW/Of4A3Yc4zto+8ONabdI=
ENCRYPTED

  end

  context 'load_keys' do
    it 'loads public key and private key' do
      public_key, private_key = EncryptionUtil.load_keys("#{@key_path}/public_key.pem", "#{@key_path}/private_key.pem")
      expect(public_key).not_to be_nil
      expect(private_key).not_to be_nil
    end
  end

  context 'encrypt' do
    # it 'raise exception if keys are not loaded' do
    #   expect {EncryptionUtil.encrypt(@text)}.to raise_error(RuntimeError, 
    #         'You have to load keys by calling EncryptionUtil.load_keys(public_key_file, private_key_file)')
    # end

    it 'encrypts plain text' do
      encrypted_text = EncryptionUtil.encrypt(@text)
      expect(encrypted_text).not_to be_nil
      expect(encrypted_text).not_to eql(@text)
    end
  end

  context 'decrypt' do
    it 'decrypts encrypted_text' do
      encrypted_text = EncryptionUtil.encrypt(@text)
      decrypted_text = EncryptionUtil.decrypt(encrypted_text)
      expect(decrypted_text).to eql(@text)
    end
  end
end

=begin

Follow these articles
http://stackoverflow.com/questions/9891708/ruby-file-encryption-decryption-with-private-public-keys
http://docs.ruby-lang.org/en/2.1.0/OpenSSL.html

Create keys:
key = OpenSSL::PKey::RSA.new 2048

open 'private_key.pem', 'w' do |io| io.write key.to_pem end
open 'public_key.pem', 'w' do |io| io.write key.public_key.to_pem end 

  
=end