require 'rails_helper'

describe UcmController do
  HTTPI.adapter = :rack
  HTTPI::Adapter::Rack.mount 'app', Animals::Application

  before :all do
    @app_root = 'http://app'
    @client = Savon::Client.new(wsdl: "#{@app_root}/ucm/wsdl")
    @client.operations
  end

  context 'wsdl' do
    it 'responses to wsdl' do
      parsed_params = Rails.application.routes.recognize_path ucm_wsdl_url
      controller = parsed_params.delete(:controller)
      action = parsed_params.delete(:action)
      get(action, parsed_params)
      expect(response).to be_success
      expect(response).to have_http_status(200)
    end

    it 'returns correct set of operations' do
      expect(@client.operations). to match_array([:update_people])
    end
  end

  context 'update_people' do
    it 'returns the success status if updating person sucessfully' do
      expect(UcmService).to receive(:update_people).once
      result = @client.call(:update_people, message: { people: [{
                                        eidm_guid: '12345',
                                        first_name: 'Albert',
                                        last_name: 'Einstein',
                                        email: 'aeinstein@universe.com',
                                        language: 'de',
                                        country: 'DE'
                                      }] })

      expect(result).not_to be_nil
      expect(result.success?).to be true  
      
    end

    it 'returns failure status if update person fails' do
      allow(UcmService).to receive(:update_people).and_raise('Something wrong')
      expect { @client.call(:update_people, message: { people: [{
                                        eidm_guid: '12345',
                                        first_name: 'Albert',
                                        last_name: 'Einstein',
                                        email: 'aeinstein@universe.com',
                                        language: 'de',
                                        country: 'DE'
                                      }] }) }.to raise_error(Savon::HTTPError)
    end
  end
end