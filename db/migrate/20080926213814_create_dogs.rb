class CreateDogs < ActiveRecord::Migration
  def self.up
    create_table :dogs do |t|
      t.column :age,      :integer
      t.column :name,     :string, :limit => 40,  :null => false
      t.column :dog_attr, :string, :limit => 60
      t.column :pack_id,  :integer
      t.timestamp
    end
    
    add_index :dogs, :pack_id
    
    dog = Dog.new(:age => 2, :name=> 'Dog 1', :dog_attr => 'Belongs to pack 1', :pack_id => 1)
    dog.save!
    
    dog = Dog.new(:age => 3, :name=> 'Dog 2', :dog_attr => 'Belongs to pack 1', :pack_id => 1)
    dog.save!
    
    dog = Dog.new(:age => 4, :name=> 'Dog 3', :dog_attr => 'Belongs to pack 1', :pack_id => 1)
    dog.save!
    
    dog = Dog.new(:age => 2, :name=> 'Dog 4', :dog_attr => 'Belongs to pack 2', :pack_id => 2)
    dog.save!
    
    dog = Dog.new(:age => 3, :name=> 'Dog 5', :dog_attr => 'Belongs to pack 2', :pack_id => 2)
    dog.save!
    
    dog = Dog.new(:age => 4, :name=> 'Dog 6', :dog_attr => 'Belongs to pack 2', :pack_id => 2)
    dog.save!

    dog = Dog.new(:age => 3, :name=> 'Dog 7', :dog_attr => 'Belongs to no pack')
    dog.save!
    
  end

  def self.down
    drop_table :dogs
  end
end
