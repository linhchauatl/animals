class CatsController < ApplicationController
  include CatsHelper
  
  def list
    @not_tabby_cats, @tabby_cats = CatsService.cat_list
    render :partial => '/cats/list', :locals => { :not_tabby_cats => @not_tabby_cats, :tabby_cats => @tabby_cats  }
  end

  def ajax_show
    cat = CatsService.find_cat_by_id(params[:id])
    render :partial => "details", :locals => {:cat => cat, :update_to => params[:update_to]}
  end
  
  
  def destroy
    CatsService.delete_cat(params[:id])
    redirect_to :action => 'list'
  end

  def edit
    @cat =CatsService. find_cat_by_id(params[:id])
  end

  def update
    update_status = CatsService.update_cat(params[:id],params[:cat], params[:claw])
    
    if update_status
      flash[:notice] = 'Cat was successfully updated.'
      redirect_to :action => 'list'
    else
      flash[:notice] = 'Cannot update the cat with information you submitted.'
      render :action => 'edit'
    end
  end

  def new
    
  end

  def create
    save_status = CatsService.save_cat(params)
    if save_status
      flash[:notice] = 'Cat was successfully created.'
      redirect_to :action => 'list'
    else
      flash[:notice] = 'Cannot create a cat with information you submitted.'
      render :action => 'new'
    end
  end
  
  def ajax_show_stripes
    @create_tabby_cat = (params[:value] == 'TabbyCat')
    render :partial => 'stripes'
  end
end
