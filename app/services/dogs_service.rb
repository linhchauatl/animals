class DogsService
  
  def self.dog_list
    dogs =  Dog.all
  end
  
  def self.find_dog_by_id(id)
    dog = Dog.find(id)
    dog.populate_dogs_pack
    dog
  end
  
  def self.delete_dog(id)
    Dog.find(id).destroy
  end
  
  def self.update_dog(id, params_dog)
    dog = Dog.find(id)
    dog.update_attributes(params_dog)
    dog
  end
  
  def self.find_existing_pack_ids
    existing_pack_ids = Dog.find_by_sql("select distinct pack_id from dogs 
                                         where pack_id is not null order by pack_id").map(&:pack_id)
  end
  
  def self.find_pack_dogs_by_pack_id(pack_id)
    pack_dogs = Dog.all.where('pack_id = ?', pack_id)
  end
  
  def self.prepare_for_edit(id)
    { dog: Dog.find(id), existing_pack_ids: find_existing_pack_ids}
  end
  
  def self.save_dog(params_dog)
    dog = Dog.new(params_dog)
    dog.save!
    dog
  end
  
end