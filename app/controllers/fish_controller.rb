class FishController < ApplicationController
  
  def language_auto_comlete
    arr_languages = ['ActionScript', 'AppleScript', 'Asp','BASIC', 'C', 'C++', 'Clojure', 'COBOL',
										 'ColdFusion', 'Erlang', 'Fortran', 'Groovy', 'Haskell', 'Java', 'JavaScript',
										 'Lisp', 'Perl', 'PHP', 'Python', 'Ruby', 'Scala', 'Scheme']
    search_term =  params[:term] || params[:q]
    result = arr_languages.select{ |x| x.downcase =~ Regexp.compile(search_term.downcase) }

    search_result = result.map { |x| {:label => "#{x}_label", :value => "#{x}_value",  :item_id => "#{x}_id"} }
    search_result = { items: search_result } if params[:q]
    
    
    logger.debug("\n\nResult = \n#{search_result.inspect}\n\n")
    render json: search_result
  end
  
  def create_asset
    result = { files: [] }
    files = params[:files]
    files.each do |uploaded_file|
      filename = uploaded_file.original_filename
      Rails.logger.error("Original filename = #{filename}")
      File.open("#{Rails.root}/public/upload/#{filename}",'wb') do |f|
        f.write(uploaded_file.read)
      end
      
      result[:files] << {
                          name: filename,
                          url: "#{request.protocol}#{request.host_with_port}/upload/#{filename}"
                        }

    end
		render json: result
  end
  
end

