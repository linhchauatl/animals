require 'rails_helper'

describe DogsHelper do
  context 'row_color' do
    it 'returns :success if the index is even' do
      expect(helper.row_color(100)).to equal(:success)
    end

    it 'returns :info if the index is odd' do
      expect(helper.row_color(101)).to equal(:info)
    end
  end
end