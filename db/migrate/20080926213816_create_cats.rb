class CreateCats < ActiveRecord::Migration
  def self.up
    create_table :cats do |t|
      t.column :age,      :integer
      t.column :name,     :string, :limit => 40, :null => false
      t.column :animal_type, :string, :limit => 40;
      t.column :cat_attr,    :string, :limit => 60
      t.column :stripes,     :integer
      t.timestamp
    end
    
    # cats
    cat = Cat.new(:age => 2, :name => 'Cat 1', :cat_attr => 'Cat 1 attr')
    cat.save!
    
    cat = Cat.new(:age => 4, :name => 'Cat 2', :cat_attr => 'Cat 2 attr')
    cat.save!

    ## tabby_cats
    cat = TabbyCat.new(:age => 3, :name => 'TabbyCat 1', :cat_attr => 'TabbyCat 1 attr', :stripes => 100)
    cat.save!
    
    cat = TabbyCat.new(:age => 5, :name => 'TabbyCat 2', :cat_attr => 'TabbyCat 2 attr', :stripes => 120)
    cat.save!
 
  end

  def self.down
    drop_table :cats
  end
end
