## animals

This is a rewrite of one of my first Rails apps.<br/>
The original program was written in Rails 1.2 around 2007, with UI written in ERB, Prototype JS and script.aculo.us.
<br/>

I wrote it during an interview. They asked me to design, write DLL and Java Hibernate mapping for the following requirement:
> **Hibernate:**
> 
> Consider the following class hierarchy of animals, with Animal at the root. Cat, Dog, and Fish are subclasses of Animal. TabbyCat is a subclass of Cat. Goldfish is a subclass of Fish.
>
> Animal has an integer property called "age".<br/>
> Cat has an byte[] property called "claws".<br/>
> TabbyCat has an integer property called "stripes".<br/>
> Dog has a List property called "pack", which lists other dogs.  
>
> Please write the DDL for the table structure and the hibernate mapping that covers this hierarchy.
> Describe any of the design decisions you make. 

<br/>
After finishing the test, I had plenty of time left, so I wrote the Rails application to display and manipulate dogs and cats, just for the heck of it. It was the time of Ruby 1.8 and Rails 1.2.

<br/>
This current app is rewritten in Rails 4 with UI written in HAML, jQuery, to demo several things:
* Twitter Bootstrap 3 (quite a revamp from the Bootstrap 2)
* Ajax Multiupload using blueimp/jQuery-File-Upload
* JQuery autocomplete with remote source
* JQuery localize datepicker
* Elastic Search for Dog

The program uses the following things:
* Ruby version 2.2.3
* Rails version 4.x.x
* PostgreSQL database. But it can run with MySQL or any other RDBMS that ActiveRecord supports. You must supply your own **database.yml**. See the file [config/database.example.yml](https://github.com/linhchauatl/animals/blob/master/config/database.example.yml) for sample.
* Database creation: `rake db:create`
* Database initialization: `rake db:migrate`
* ElasticSearch: You must have an ElasticSearch server running somewhere. You must modify the file [config/elasticsearch.example.yml](https://github.com/linhchauatl/animals/blob/master/config/elasticsearch.example.yml) to create a file **config/elasticsearch.yml** that points to your ElasticSearch server.
* If you don't want to use ElasticSearch, just rename the file [config/elasticsearch.example.yml](https://github.com/linhchauatl/animals/blob/master/config/elasticsearch.example.yml) to **config/elasticsearch.yml**.

![alt text](https://github.com/linhchauatl/animals/blob/master/public/images/snow_leopard.jpg "Snow Leopard")
