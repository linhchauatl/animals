// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require jquery-ui
//= require_tree .
//= require bootstrap-sprockets
//= require jquery-ui/datepicker-af.js
//= require jquery-ui/datepicker-ar-DZ.js
//= require jquery-ui/datepicker-ar.js
//= require jquery-ui/datepicker-az.js
//= require jquery-ui/datepicker-be.js
//= require jquery-ui/datepicker-bg.js
//= require jquery-ui/datepicker-bs.js
//= require jquery-ui/datepicker-ca.js
//= require jquery-ui/datepicker-cs.js
//= require jquery-ui/datepicker-cy-GB.js
//= require jquery-ui/datepicker-da.js
//= require jquery-ui/datepicker-de.js
//= require jquery-ui/datepicker-el.js
//= require jquery-ui/datepicker-en-AU.js
//= require jquery-ui/datepicker-en-GB.js
//= require jquery-ui/datepicker-en-NZ.js
//= require jquery-ui/datepicker-eo.js
//= require jquery-ui/datepicker-es.js
//= require jquery-ui/datepicker-et.js
//= require jquery-ui/datepicker-eu.js
//= require jquery-ui/datepicker-fa.js
//= require jquery-ui/datepicker-fi.js
//= require jquery-ui/datepicker-fo.js
//= require jquery-ui/datepicker-fr-CA.js
//= require jquery-ui/datepicker-fr-CH.js
//= require jquery-ui/datepicker-fr.js
//= require jquery-ui/datepicker-gl.js
//= require jquery-ui/datepicker-he.js
//= require jquery-ui/datepicker-hi.js
//= require jquery-ui/datepicker-hr.js
//= require jquery-ui/datepicker-hu.js
//= require jquery-ui/datepicker-hy.js
//= require jquery-ui/datepicker-id.js
//= require jquery-ui/datepicker-is.js
//= require jquery-ui/datepicker-it-CH.js
//= require jquery-ui/datepicker-it.js
//= require jquery-ui/datepicker-ja.js
//= require jquery-ui/datepicker-ka.js
//= require jquery-ui/datepicker-kk.js
//= require jquery-ui/datepicker-km.js
//= require jquery-ui/datepicker-ko.js
//= require jquery-ui/datepicker-ky.js
//= require jquery-ui/datepicker-lb.js
//= require jquery-ui/datepicker-lt.js
//= require jquery-ui/datepicker-lv.js
//= require jquery-ui/datepicker-mk.js
//= require jquery-ui/datepicker-ml.js
//= require jquery-ui/datepicker-ms.js
//= require jquery-ui/datepicker-nb.js
//= require jquery-ui/datepicker-nl-BE.js
//= require jquery-ui/datepicker-nl.js
//= require jquery-ui/datepicker-nn.js
//= require jquery-ui/datepicker-no.js
//= require jquery-ui/datepicker-pl.js
//= require jquery-ui/datepicker-pt-BR.js
//= require jquery-ui/datepicker-pt.js
//= require jquery-ui/datepicker-rm.js
//= require jquery-ui/datepicker-ro.js
//= require jquery-ui/datepicker-ru.js
//= require jquery-ui/datepicker-sk.js
//= require jquery-ui/datepicker-sl.js
//= require jquery-ui/datepicker-sq.js
//= require jquery-ui/datepicker-sr-SR.js
//= require jquery-ui/datepicker-sr.js
//= require jquery-ui/datepicker-sv.js
//= require jquery-ui/datepicker-ta.js
//= require jquery-ui/datepicker-th.js
//= require jquery-ui/datepicker-tj.js
//= require jquery-ui/datepicker-tr.js
//= require jquery-ui/datepicker-uk.js
//= require jquery-ui/datepicker-vi.js
//= require jquery-ui/datepicker-zh-CN.js
//= require jquery-ui/datepicker-zh-HK.js
//= require jquery-ui/datepicker-zh-TW.js