# Methods added to this helper will be available to all templates in the application.
module ApplicationHelper
  def spinner_url
    image_path('indicator.gif')
  end
  
  def spinner(alt = 'Loading...')
    icon('/indicator.gif', nil, alt)
  end

  def spinner_text(text = 'Loading...')
    String(text) + icon('/indicator.gif', nil, text)
  end

  def centered_spinner_html
    '<div class="vCenteredDivWayOuter"><div class="vCenteredDivOuter"><div class="vCenteredDivInner">' + spinner_text + '</div></div></div>'
  end
  
  #
  # icon / image methods
  #
  
  def icon_path(icon_name)
    if icon_name[0,1] == '/'
      icon_name[1..icon_name.size]            
    else      
        "icons/#{icon_name}.png"
    end
  end

  def icon(icon_name, id=nil, title=nil, html_options = {})
    return spacer(16,16) if icon_name == 'blank' && !id && !title    
    
    if html_options.keys.include?('escape_quote')
      html_safe = html_options['escape_quote']
      html_options.delete('escape_quote') 
    end
    
    options = { 'border' => 0 }.merge(html_options)
    options['id'] = id if id
    options['alt'] = options['title'] = title if title    
    src = icon_path(icon_name)        
    if src[-3,3] == 'png' && @request.env['HTTP_USER_AGENT'] =~ /MSIE 6\.0/
      
      # this line is needed because when img path is rendered
      # inside AlphaImageLoader, it takes the path relative to the 
      # form where it was rendered -- need to take the absolute path of icons         
      
      src[0,5] == 'icons' ? src = '/themes/shared/images/'.concat(src) : src = '/'.concat(src)
            
      # when rendering any elements with quotes inside innerHTML,
      # quotes need to be converted into html safe quotes
      # so as not to break the string - this is only for IE 6.x      
      
      safe_quote = html_safe ? "&#39;"  : "'"   
               
      # MSIE png transparency hack
      options[:style] = "filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(enabled=true, sizingMethod=#{safe_quote}scale#{safe_quote}, src=#{safe_quote}#{src}#{safe_quote});"            
      options[:height] = 16
      options[:width] = 16
      options[:alt] ||= icon_name                  
      image_tag('spacer.gif', options)
    else      
      image_tag(src, options)
    end
  end
  
end
