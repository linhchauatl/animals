require 'rails_helper'

describe DogsController do
  context 'index' do
    before :each do
      # RSpec 3 syntax
      allow(DogsService).to receive(:dog_list).and_return([])
      allow(CatsService).to receive(:cat_list).and_return([ [], [] ])
    end

    it 'renders template index' do
      get :index
      expect(response).to be_success
      expect(response).to have_http_status(200)
      expect(response).to render_template(:index) 
    end
  end

  context 'ajax_show' do
    before :all do
      @show_dog = create(:dog, pack_id: 2)
      @dog_1 = create(:dog, name: 'Dog 1', pack_id: 2)
      @dog_2 = create(:dog, name: 'Dog 2', pack_id: 2)
    end

    it 'renders partial dogs/_details' do
      get :ajax_show, id: @show_dog.id
      expect(response).to be_success
      expect(response).to have_http_status(200)
      expect(response).to render_template('dogs/_details') 
    end

    it 'calls DogService.find_dog_by_id' do
      expect(DogsService).to receive(:find_dog_by_id).with(@show_dog.id).and_return(@show_dog)
      get :ajax_show, id: @show_dog.id
    end

    # This test is not necessary, because we will test DogsService implementation in dogs_service_spec.rb
    # I keep it there to illustrate the new syntax of any_instance
    it 'calls populate_dogs_pack if the dog is found' do
      expect_any_instance_of(Dog).to receive(:populate_dogs_pack) do |dog|
        dog.dogs_pack = [@dog_1, @dog_2]
      end

      get :ajax_show, id: @show_dog.id
    end
  end

  context 'ajax_pack_show' do
    before :all do
      @dog_1 = create(:dog, name: 'Dog 1', pack_id: 4)
      @dog_2 = create(:dog, name: 'Dog 2', pack_id: 4)
    end

    it 'renders partial dogs/_pack_details' do
      get :ajax_pack_show, id: @dog_1.pack_id
      expect(response).to be_success
      expect(response).to have_http_status(200)
      expect(response).to render_template('dogs/_pack_details') 
    end

    it 'calls DogService.find_pack_dogs_by_pack_id' do
      expect(DogsService).to receive(:find_pack_dogs_by_pack_id).with(@dog_1.pack_id).and_return([@dog_1, @dog_2])
      get :ajax_pack_show, id: @dog_1.pack_id
    end
  end

  context 'destroy' do
    before :each do
      @dog = create(:dog, name: 'Will be destroyed Dog', pack_id: 6)
    end

    it 'redirects to action index' do
      delete :destroy, id: @dog.id
      expect(response).to redirect_to('/')
    end

    it 'calls DogService.delete_dog' do
      expect(DogsService).to receive(:delete_dog).with(@dog.id).and_return(@dog)
      delete :destroy, id: @dog.id
    end
  end

  context 'edit' do
    before :each do
      @dog = create(:dog)
    end

    it 'renders template dogs/edit' do
      get :edit, id: @dog.id
      expect(response).to be_success
      expect(response).to have_http_status(200)
      expect(response).to render_template('dogs/edit')

      # Indirectly test the private prepare_for_edit
      expect(assigns(:dog).id).to equal(@dog.id)
      expect(assigns(:existing_pack_ids)).to match_array(Dog.all.map(&:pack_id).uniq)
    end

    it 'calls prepare_for_edit' do
      expect(controller).to receive(:prepare_for_edit).with(@dog.id)
      get :edit, id: @dog.id
    end
  end

  context 'update' do
    before :each do
      @dog = create(:dog)
      @params = { dog: { name: 'Dog update 1', age: '4', dog_attr: 'Belongs to pack 100', pack_id: '100'}, 
                  id: @dog.id
                }
    end

    it 'calls DogsService.update_dog(params[:id].to_i, dog_params)' do
      expect(DogsService).to receive(:update_dog).with(@dog.id, @params[:dog].stringify_keys)
      put :update, @params
    end

    context 'success' do
      it 'redirects to /' do
        put :update, @params
        expect(response).to be_redirect
        expect(response).to have_http_status(302)
        expect(response).to redirect_to('/')
      end
    end

    context 'failure' do
      it 'renders action edit' do
        allow(DogsService).to receive(:update_dog).and_return(false)
        put :update, @params
        expect(response).to be_success
        expect(response).to have_http_status(200)
        expect(response).to render_template('dogs/edit')

        # Indirectly test the private prepare_for_edit
        expect(assigns(:dog).id).to equal(@dog.id)
        expect(assigns(:existing_pack_ids)).to match_array(Dog.all.map(&:pack_id).uniq)
      end

      it 'calls prepare_for_edit' do
        expect(controller).to receive(:prepare_for_edit).with(@dog.id)
        allow(DogsService).to receive(:update_dog).and_return(false)
        put :update, @params
      end
    end
  end

  context 'new' do
    it 'renders template dogs/new' do
      get :new
      expect(response).to be_success
      expect(response).to have_http_status(200)
      expect(response).to render_template('dogs/new')
    end

    it 'calls DogsService.find_existing_pack_ids' do
      expect(DogsService).to receive(:find_existing_pack_ids).and_return([])
      get :new
    end
  end

  context 'create' do
    before :all do
      @params = { dog: {name: 'Dog 8', age: '8', dog_attr: 'Belongs to pack 3', pack_id: '3'} }
    end

    it 'calls DogsService.save_dog(dog_params)' do
      expect(DogsService).to receive(:save_dog).with(@params[:dog].stringify_keys)
      post :create, @params
    end

    context 'success' do
      it 'redirects to /' do
        post :create, @params
        expect(response).to be_redirect
        expect(response).to have_http_status(302)
        expect(response).to redirect_to('/')
      end
    end

    context 'failure' do
      it 'renders action new' do
        allow(DogsService).to receive(:save_dog).and_return(false)
        post :create, @params
        expect(response).to be_success
        expect(response).to have_http_status(200)
        expect(response).to render_template('dogs/new')
      end

      it 'calls DogsService.find_existing_pack_ids' do
        expect(DogsService).to receive(:find_existing_pack_ids).and_return([])
        allow(DogsService).to receive(:save_dog).and_return(false)
        post :create, @params
      end
    end
  end

end