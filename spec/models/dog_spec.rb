require 'rails_helper'

describe Dog do

  before :all do
    @dog = create(:dog)
  end

  context 'validation' do
    context 'name' do 
      it 'is invalid without a name' do
        expect(build(:dog, name: nil)).not_to be_valid
      end

      it 'is invalid with an empty name' do
        expect(build(:dog, name: '')).not_to be_valid
      end

      it 'is invalid with a name longer than 40' do
        expect(build(:dog, name: 'a' * 41)).not_to be_valid
      end
    end
    
    context 'age' do
      it 'is invalid without an age' do 
        expect(build(:dog, age: nil)).not_to be_valid 
      end

      it 'is invalid if age is not a number' do 
        expect(build(:dog, age: 'age')).not_to be_valid 
      end
    end

    context 'pack_id' do
      it 'is invalid if pack_id is not a number' do 
        expect(build(:dog, pack_id: 'pack_id')).not_to be_valid 
      end
    end
    
    context 'dog_attr' do
      it 'is invalid with a dog attr longer than 60' do
        expect(build(:dog, dog_attr: 'a' * 61)).not_to be_valid
      end
    end
  end

  context 'accessor' do
    it 'respond to dogs_pack' do
      expect(@dog.respond_to?(:dogs_pack)).to be true
    end

    it 'respond to dogs_pack=' do
      expect(@dog.respond_to?(:dogs_pack=)).to be true
    end

    it 'respond to populate_dogs_pack' do
      expect(@dog.respond_to?(:populate_dogs_pack)).to be true
    end
  end

  context 'populate_dogs_pack' do
    it 'returns all dogs of the same pack except the callee' do
      dog_1 = create(:dog, name: 'Dog 1', pack_id: 5)
      dog_2 = create(:dog, name: 'Dog 2', pack_id: 5)
      dogs = dog_1.populate_dogs_pack
      expect(dogs).to match_array([dog_2])
    end

    it 'returns an empty array if the callee is the only dog in the pack' do
      dog_1 = create(:dog, name: 'Dog 1', pack_id: 7)
      dogs = dog_1.populate_dogs_pack
      expect(dogs).to match_array([])
    end

    it 'returns an empty array if the callee has no pack_id' do
      dog_1 = create(:dog, name: 'Dog 1', pack_id: nil)
      dogs = dog_1.populate_dogs_pack
      expect(dogs).to match_array([])
    end
  end
end