class CreateClaws < ActiveRecord::Migration
  def self.up
    create_table :claws do |t|
      t.column :cat_id,     :integer, :null => false
      t.column :claw_value, :integer, :null => false
      t.timestamp
    end
    
    add_index :claws, :cat_id
    
    ## find all cats
    cats = Cat.all
    
    cats.each { |cat|
      # create 4 claws for each cat
      4.times do 
        claw = Claw.new(:claw_value => rand(20))
        claw.cat = cat
        claw.save!
      end
    }

  end

  def self.down
    drop_table :claws
  end
end
