module DogsHelper
  def row_color(idx)
    colors = [:success, :info]
    colors[idx % 2]
  end
end
